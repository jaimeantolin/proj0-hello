# Proj0-Hello
-------------

**Author:**  Jaime Antolin Merino

**Contact Address:**  jaimea@uoregon.edu

**Description:**  First project for CIS322 class to familiarize ourselves with the use of git as well as how submissions for the class will be made. Code outputs a message that is taken from our credentials.ini file which in this case is "Hello world".


